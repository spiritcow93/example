import 'package:flutter_atoms/blocs/blocs.dart';
import 'package:get_it/get_it.dart';
import 'package:help_care/blocs/detailed_client_service/detailed_client_service_cubit.dart';
import 'package:help_care/extension/ext_text.dart';
import 'package:help_care/models/client_named_service.dart';
import 'package:help_care/providers/cached_contacts_provider.dart';
import 'package:intl/intl.dart';
import 'package:sso_client_api/model/person_response.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../setup.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import "package:collection/collection.dart";

import 'detailed_client_service_with_headings.dart';

class DetailedClientServiceScreen extends StatelessWidget {
  final PersonResponse currentUser;
  DetailedClientServiceScreen(this.currentUser);
  //TODO хардкод список ограничений
  final List<String> limitations = ['Без ограничений'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: BlocProvider<DetailedClientServiceCubit>(
        create: (_) {
          return DetailedClientServiceCubit(sl(), sl(), sl(), sl())
            ..load(currentUser);
        },
        child:
            BlocBuilder<DetailedClientServiceCubit, DetailedClientServiceState>(
                builder: (context, state) {
          return SingleChildScrollView(
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //TODO переделать limitationWidget
                makeHeaderWidget(context, currentUser),
                makeLimitationsWidget(context, currentUser),
                makeListWithDetailedTasks(
                    context, state.data.clientServices, state)
              ],
            ),
          );
        }),
      ),
    );
  }

  Widget makeHeaderWidget(BuildContext context, PersonResponse currentUser) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            makeAvatarSubWidget(currentUser),
            makeMainInfoHeaderSubWidget(context, currentUser)
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Divider(
          color: Colors.grey,
          height: 2,
        )
      ],
    );
  }

  Widget makeLimitationsWidget(
      BuildContext context, PersonResponse currentUser) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 15, bottom: 10),
          child: Text('Ограничения'),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 20),
          child: Wrap(
            children: limitations
                .map((item) => Padding(
                      padding: const EdgeInsets.only(right: 5),
                      child: Material(
                        borderRadius: BorderRadius.circular(5),
                        elevation: 2,
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 5, bottom: 5, left: 10, right: 10),
                          decoration: BoxDecoration(
                            color:
                                Theme.of(context).brightness == Brightness.dark
                                    ? Colors.black26
                                    : Colors.white,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: AutoSizeText(
                            item,
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                        ),
                      ),
                    ))
                .toList(),
          ),
        ),
        Divider(
          color: Colors.grey,
          height: 2,
        )
      ],
    );
  }

  Widget makeAvatarSubWidget(PersonResponse currentUser) {
    return Padding(
      padding: const EdgeInsets.only(right: 10),
      child: CircleAvatar(
        radius: 40,
        backgroundImage: NetworkImage(
          currentUser.photo,
        ),
      ),
    );
  }

  Widget makeMainInfoHeaderSubWidget(
      BuildContext context, PersonResponse currentUser) {
    print("currentUser.id: ${currentUser.id}");
    var contact =
        GetIt.instance<CachedContactsProvider>().id2contacts[currentUser.id];
    var phoneNumber =
        contact != null ? contact.phoneNumber : 'Нет контактных данных';
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //TODO На беке нет болезни
          makeNameSubWidget(context, currentUser),
          makeAddressSubWidget(context, currentUser.address),
          makeDiseaseButtonSubWidget(context, 'Заболеваний не выявлено'),
          makeRowWithDateAndGenderSubWidget(context, currentUser),
          InkWell(
            child: makeRowWithDataSubWidget(context, Icons.phone, phoneNumber),
            onTap: () {
              launch("tel:$phoneNumber");
            },
          ),
        ],
      ),
    );
  }

  Widget makeNameSubWidget(BuildContext context, PersonResponse currentUser) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 5),
      child: Text(
        currentUser.name +
            ' ' +
            currentUser.middleName +
            ' ' +
            currentUser.surname,
      ),
    );
  }

  Widget makeRowWithDateAndGenderSubWidget(
      BuildContext context, PersonResponse currentUser) {
    return Row(
      children: [
        makeRowWithDataSubWidget(
          context,
          Icons.calendar_today_outlined,
          currentUser.details.birthDay != null
              ? DateFormat('dd MMM yyyy').format(currentUser.details.birthDay)
              : 'не задано',
        ),
        SizedBox(
          width: 40,
        ),
        Expanded(
          child: makeRowWithDataSubWidget(
              context,
              Icons.person,
              getGender(currentUser.details.gender != null
                  ? currentUser.details.gender
                  : 'не задано')),
        ),
      ],
    );
  }

  Widget makeAddressSubWidget(BuildContext context, String text) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Row(children: [
        Icon(
          Icons.location_on,
          color: Colors.blue,
          size: 20,
        ),
        SizedBox(
          width: 10,
        ),
        Flexible(
            child: AutoSizeText(
          text ?? 'не задано',
          style: Theme.of(context).textTheme.subtitle1,
        )),
        InkWell(
            child: Icon(
              Icons.chevron_right,
              color: Color(0xFF8491A7),
            ),
            onTap: () {
              //TODO Метод при клике на адресс в детальной странице пользователя
            }),
      ]),
    );
  }

  Widget makeRowWithDataSubWidget(
      BuildContext context, IconData icon, String text) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        children: [
          Icon(
            icon,
            color: Color(0xFF8491A7),
          ),
          SizedBox(
            width: 5,
          ),
          AutoSizeText(
            text,
            style: Theme.of(context).textTheme.subtitle1,
          )
        ],
      ),
    );
  }

  Widget makeDiseaseButtonSubWidget(BuildContext context, String text) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: InkWell(
          child: Container(
            width: MediaQuery.of(context).size.width / 1.5,
            padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Theme.of(context).brightness == Brightness.dark
                    ? Colors.black26
                    : Colors.grey[100],
                border: Border.all(color: Colors.grey)),
            child: AutoSizeText(
              text,
              textAlign: TextAlign.start,
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          onTap: () {
            //TODO Метод при клике на болезнь в детальной странице пользователя
          }),
    );
  }

  Widget makeListWithDetailedTasks(
      BuildContext context,
      List<ClientNamedService> listOfDetailedTasks,
      DetailedClientServiceState state) {
    List<ListItem> listOfDetailedTasksFiltered = [];

    groupBy(listOfDetailedTasks, (ClientNamedService obj) => obj.time)
        .forEach((key, value) {
      listOfDetailedTasksFiltered.add(HeadingItem(key.toString(), null));
      value.forEach((element) {
        listOfDetailedTasksFiltered.add(MessageItem(element));
      });
    });
    return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: listOfDetailedTasksFiltered.length,
        itemBuilder: (BuildContext context, index) {
          final item = listOfDetailedTasksFiltered[index];
          return item.buildHeading(context) != null
              ? item.buildHeading(context)
              : item.buildBody(context,
                  isChecked: item.body.actualQuantity == item.body.quantity
                      ? true
                      : false,
                  onChanged: (value) {
                    BlocProvider.of<DetailedClientServiceCubit>(context)
                        .completeTask(item.body, currentUser, value, index);
                  },
                  onMinus: () =>
                      BlocProvider.of<DetailedClientServiceCubit>(context)
                          .completeTask(item.body, currentUser, false, index),
                  onPlus: () =>
                      BlocProvider.of<DetailedClientServiceCubit>(context)
                          .completeTask(item.body, currentUser, true, index),
                  currentListIndex: index,
                  currentClickedItemIndex: state.data.currentIndex,
                  state: state);
        });
  }
}
