import 'package:bloc/bloc.dart';
import 'package:help_care/constants/auth.dart';
import 'package:help_care/models/api.dart';
import 'package:help_care/models/client_named_service.dart';
import 'package:help_care/providers/cached_auth_provider.dart';
import 'package:help_care/providers/cached_services_provider.dart';
import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';
import 'package:service_client_api/model/bulk_complete_task_services.dart';
import 'package:sso_client_api/model/authenticated_person.dart';
import 'package:sso_client_api/model/person_response.dart';

part 'detailed_client_service_state.dart';

@injectable
class DetailedClientServiceCubit extends Cubit<DetailedClientServiceState> {
  final CachedAuthProvider authProvider;
  final CachedServicesProvider serviceProvider;
  final API api;
  final OAuthInterceptor oauth;

  DetailedClientServiceCubit(
      this.api, this.oauth, this.authProvider, this.serviceProvider)
      : super(DetailedClientServiceState(
            DetailedClientServiceData(), DetailedClientServiceStateType.INIT));

  Future<void> load(PersonResponse currentClient) async {
    emit(DetailedClientServiceState(
        state.data, DetailedClientServiceStateType.LOADING));

    var currentPerson =
        authProvider.token2person[oauth.tokens[AuthKeys.defaultProvider]];

    final now = new DateTime.now();
    state.data.startDate = state.data.endDate = now;

    await getAssigneeClientServices(currentPerson, currentClient);

    emit(DetailedClientServiceState(
        state.data, DetailedClientServiceStateType.LOADED));
  }

  Future<void> completeTask(ClientNamedService service,
      PersonResponse currentClient, bool isPlus, int currentIndex) async {
    state.data.currentIndex = currentIndex;
    emit(DetailedClientServiceState(
        state.data, DetailedClientServiceStateType.LOADING));

    var currentPerson =
        authProvider.token2person[oauth.tokens[AuthKeys.defaultProvider]];

    List<String> currentServices = [];
    List<String> currentClients = [];

    currentServices.add(service.service.id);
    currentClients.add(currentClient.id);

    var response = await api.serviceApi
        .getTaskServicesApi()
        .appV1TaskServicesBulkComplete(
            bulkCompleteTaskServices: BulkCompleteTaskServices(
                completionStrategy: isPlus ? 'plus-one' : 'minus-one',
                assigneeId: currentPerson.id,
                serviceIds: currentServices,
                clientIds: currentClients,
                startDate: state.data.startDate,
                endDate: state.data.endDate));

    print(response);

    await getAssigneeClientServices(currentPerson, currentClient);

    emit(DetailedClientServiceState(
        state.data, DetailedClientServiceStateType.LOADED));
  }

  Future<void> getAssigneeClientServices(
      AuthenticatedPerson currentPerson, PersonResponse currentClient) async {
    var assigneeClientServices = await api.mainApi
        .apiV1AssigneeClientServicesList(
            ordering: "service__name",
            assigneeIdIn: currentPerson.id,
            clientIdIn: currentClient.id,
            startDateExact: state.data.startDate,
            endDateExact: state.data.endDate,
            pageLeftSquareBracketSizeRightSquareBracket: 1000);

    List<ClientNamedService> namedServices = [];

    assigneeClientServices.results.forEach((element) {
      var startTime = DateFormat('HH:mm:ss')
          .parse(element.startTime)
          .add(const Duration(hours: 4));
      var endTime = DateFormat('HH:mm:ss')
          .parse(element.endTime)
          .add(const Duration(hours: 4));
      namedServices.add(ClientNamedService(
          serviceProvider.id2service[element.serviceId],
          DateFormat('HH:mm').format(startTime) +
              ' - ' +
              DateFormat('HH:mm').format(endTime),
          element.actualQuantity,
          element.quantity));
    });

    var amount = assigneeClientServices.results.length;
    print("amount of client ${currentClient.id} services: $amount");
    state.data.clientServices = namedServices;
  }
}
