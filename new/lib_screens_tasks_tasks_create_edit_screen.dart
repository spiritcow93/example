import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_atoms/flutter_atoms.dart';
import 'package:helpcare_admin_portal/blocs/route_arguments/route_arguments_cubit.dart';
import 'package:helpcare_admin_portal/blocs/tasks/tasks_list_cubit.dart';
import 'package:helpcare_admin_portal/blocs/tasks/tasks_list_item_cubit.dart';
import 'package:helpcare_admin_portal/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:helpcare_admin_portal/blocs/search/search_popup_bloc.dart';
import 'package:helpcare_admin_portal/blocs/tasks/tasks_crud_cubit.dart';
import 'package:helpcare_admin_portal/models/tasks/task.dart';
import 'package:helpcare_admin_portal/ui/dialog_search.dart';
import 'package:intl/intl.dart';
import 'package:main_api_client/model/service.dart';

import '../../setup.dart';

//todo: Верстка

class TasksCreateEditScreen extends StatelessWidget {
  var cubit = TasksCrudCubit(sl(), sl(), sl());

  var _rootScrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    TaskModel? task = sl<RouteArgumentsCubit>()
        .getArgument(ModalRoute.of(context)!.settings.name!) as TaskModel?;

    if (task != null) {
      cubit
        ..state.data.currentEmployee = task.employee
        ..state.data.currentClient = task.client
        ..state.data.services = task.services!
        ..state.data.selectedStartDate = task.startDate
        ..state.data.selectedEndDate = task.endDate
        ..state.data.isEditing = true
        ..state.data.currentTaskId = task.id
        ..state.data.currentTask = task;
    }
    return Scaffold(
        appBar: AppBar(
          actions: [
            CircleAvatar(
              backgroundImage: AssetImage(Images.female_avatar),
            ),
          ],
        ),
        body: BlocBuilder<TasksCrudCubit, TasksCrudState>(
          bloc: cubit,
          builder: (context, state) {
            return cubit.state.type == TasksCrudStateType.TASK_CREATING
                ? Center(child: CircularProgressIndicator())
                : Padding(
                    padding:
                        const EdgeInsets.only(top: 60, left: 30, right: 30),
                    child: SingleChildScrollView(
                      controller: _rootScrollController,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 25),
                            child: Text(
                                state.data.isEditing
                                    ? 'Редактирование задания'
                                    : 'Добавление задания',
                                style: TextStyle(
                                    color: Color(0xFF252733),
                                    fontSize: 24,
                                    fontWeight: FontWeight.w700)),
                          ),
                          makeRowWithPersonAndServiceDataWidget(
                            type: 'client',
                            titleText: '',
                            context: context,
                            state: state,
                            //TODO метод для выбора клииента
                            onTap: () =>
                                DialogSearch('Клиенты', DialogType.CLIENTS)
                                    .layoutDialog(context)
                                    .then((value) {
                              if (value != null) cubit.getClient(value);
                            }),
                            placeHolderText: 'Выбрать клиента',
                            changeButtonText: 'Изменить клиента',
                            currentPersonOrService: state.data.currentClient !=
                                    null
                                ? '${state.data.currentClient!.surname} ${state.data.currentClient!.name} ${state.data.currentClient!.middleName}'
                                : '',
                          ),
                          !state.data.validationClient
                              ? Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 10),
                                  child: Text(
                                    state.data.validationClientError,
                                    style: TextStyle(color: Colors.red),
                                  ),
                                )
                              : Container(),
                          makeDatePickersWidget(context, state),
                          makeRowWithPersonAndServiceDataWidget(
                            type: 'employee',
                            titleText: 'Исполнитель',
                            context: context,
                            state: state,
                            //TODO метод для выбора исполнителя
                            onTap: () =>
                                DialogSearch('Сотрудники', DialogType.EMPLOYEES)
                                    .layoutDialog(context)
                                    .then((value) {
                              if (value != null) cubit.getEmployee(value);
                            }),
                            placeHolderText: 'Выбрать исполнителя',
                            changeButtonText: 'Изменить исполнителя',
                            currentPersonOrService: state
                                        .data.currentEmployee !=
                                    null
                                ? '${state.data.currentEmployee!.surname} ${state.data.currentEmployee!.name} ${state.data.currentEmployee!.middleName}'
                                : '',
                          ),
                          !state.data.validationEmployee
                              ? Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 10),
                                  child: Text(
                                    state.data.validationEmployeeError,
                                    style: TextStyle(color: Colors.red),
                                  ),
                                )
                              : Container(),
                          makeRowWithPersonAndServiceDataWidget(
                            type: 'service',
                            titleText: 'Услуги',
                            context: context,
                            state: state,
                            onTap: () =>
                                DialogSearch('Услуги', DialogType.SERVICE)
                                    .layoutDialog(context)
                                    .then((value) => {
                                          if (value != null)
                                            cubit.getTask((value as Service).id)
                                        }),
                            placeHolderText: state.data.services.isEmpty
                                ? 'Выбрать услугу'
                                : 'Добавить услугу',
                            changeButtonText: 'Изменить услугу',
                            currentPersonOrService: state.data.currentServices,
                          ),
                          !state.data.validationService
                              ? Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 10),
                                  child: Text(
                                    state.data.validationServiceError,
                                    style: TextStyle(color: Colors.red),
                                  ),
                                )
                              : Container(),
                          Stack(
                            children: [
                              makeServicesList(state.data.services),
                              Positioned(
                                  bottom:
                                      MediaQuery.of(context).size.height / 1.07,
                                  child: makeRowWithButtons(context)),
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
          },
        ));
  }

  Widget makeDatePickersWidget(BuildContext context, TasksCrudState state) {
    Map<String, String> datePickerTitles = {
      'start': 'Дата начала',
      'end': 'Дата окончания'
    };

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16),
          child: Text(
            'Периоды',
            style: TextStyle(
                fontSize: 16, color: Colors.black, fontWeight: FontWeight.w600),
          ),
        ),
        Row(
            children: datePickerTitles.entries
                .map((dateEntry) => Padding(
                      padding: const EdgeInsets.only(right: 20, bottom: 12),
                      child: Container(
                        padding: EdgeInsets.all(12),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: state.data.validationDate == false
                                  ? Colors.red
                                  : Color(0xFF1890FF)),
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 20,
                            ),
                            Text(
                              state.data.selectedStartDate != null &&
                                      dateEntry.key == 'start'
                                  ? DateFormat("dd MMMM yyyy")
                                      .format(state.data.selectedStartDate!)
                                  : state.data.selectedEndDate != null &&
                                          dateEntry.key == 'end'
                                      ? DateFormat("dd MMMM yyyy")
                                          .format(state.data.selectedEndDate!)
                                      : dateEntry.value,
                              style: TextStyle(
                                  color: Color(0xFF1890FF),
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            InkWell(
                              onTap: () async {
                                DateTime? picked = await showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(2000),
                                  lastDate: DateTime(2025),
                                );

                                if (picked != null &&
                                    picked !=
                                        (dateEntry.key == 'start'
                                            ? state.data.selectedStartDate
                                            : state.data.selectedEndDate)) {
                                  picked = DateTime(picked.year, picked.month,
                                      picked.day, 4, 0, 0);

                                  cubit.changeDate(
                                      dateEntry.key == 'start' ? true : false,
                                      picked);
                                }
                              },
                              child: SvgPicture.asset(
                                'assets/tasks/date_picker.svg',
                                height: 15,
                                width: 15,
                              ),
                            )
                          ],
                        ),
                      ),
                    ))
                .toList()),
        state.data.validationDate == false
            ? Text(
                state.data.validationDateError,
                style: TextStyle(color: Colors.red),
              )
            : Container(),
      ],
    );
  }

  Widget makeRowWithPersonAndServiceDataWidget(
      {required BuildContext context,
      required String currentPersonOrService,
      required TasksCrudState state,
      required Function() onTap,
      required String placeHolderText,
      required String changeButtonText,
      required String titleText,
      required String type}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        titleText.isNotEmpty
            ? Padding(
                padding: const EdgeInsets.symmetric(vertical: 12),
                child: Text(
                  titleText,
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.w600),
                ),
              )
            : Container(),
        InkWell(
          onTap: () {
            onTap();
          },
          child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(
                    color: !state.data.validationClient && type == 'client' ||
                            !state.data.validationEmployee &&
                                type == 'employee' ||
                            !state.data.validationService && type == 'service'
                        ? Colors.red
                        : Color(0xFFE3E7F6)),
              ),
              child: currentPersonOrService.isNotEmpty
                  ? makeActivePersonOrServiceWidget(
                      currentPersonOrService, changeButtonText, onTap)
                  : makeAddPersonOrServiceWidget(placeHolderText)),
        ),
      ],
    );
  }

  Widget makeRowWithButtons(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          cubit.state.data.validationFinalError.isEmpty
              ? Container()
              : Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    cubit.state.data.validationFinalError,
                    style: TextStyle(color: Colors.red),
                  ),
                ),
          Row(
            children: [
              TextButton(
                style: ButtonStyle(
                  padding: MaterialStateProperty.all(
                      EdgeInsets.symmetric(horizontal: 20)),
                  side: MaterialStateProperty.all(
                    BorderSide(
                      color: Color(0xFF1890FF),
                      width: 1,
                    ),
                  ),
                ),
                onPressed: () {
                  compass.back();
                },
                child: Text(
                  'Отмена',
                  style: TextStyle(
                      color: Color(0xFF1890FF),
                      fontSize: 13,
                      fontWeight: FontWeight.w500),
                ),
              ),
              SizedBox(
                width: 20,
              ),
              TextButton(
                style: ButtonStyle(
                  padding: MaterialStateProperty.all(
                      EdgeInsets.symmetric(horizontal: 20)),
                  side: MaterialStateProperty.all(
                    BorderSide(
                      color: Color(0xFF1890FF),
                      width: 1,
                    ),
                  ),
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Color(0xFF1890FF)),
                ),
                onPressed: () {
                  cubit.createTask().then((value) {
                    if (cubit.state.data.validationFinal) {
                      sl<TasksListCubit>()
                          .state
                          .data
                          .openedTaskListItemCubits
                          .entries
                          .forEach((element) {
                        element.value.emit(TasksListItemState(
                            element.value.state.data,
                            TasksListItemStateType.NEED_UPDATE));
                      });
                      compass.back();
                    } else {
                      _rootScrollController.jumpTo(
                          _rootScrollController.position.minScrollExtent);
                    }
                  });
                },
                child: Text(
                  'Сохранить',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 13,
                      fontWeight: FontWeight.w500),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget makeServicesList(
      Map<CustomServiceCategory, List<CustomService>> categoryToServices) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: categoryToServices.entries.length,
          itemBuilder: (context, index) {
            var currentCategory = categoryToServices.entries.elementAt(index);

            var totalCategoryQuantity = 0;
            currentCategory.value.forEach((customService) {
              totalCategoryQuantity += customService.quantity;
            });

            return Theme(
              data: ThemeData().copyWith(
                dividerColor: Colors.transparent,
              ),
              child: Container(
                padding: EdgeInsets.zero,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Color(0xFFE3E7F6)),
                ),
                child: ListTileTheme(
                  minVerticalPadding: 0,
                  contentPadding: EdgeInsets.only(right: 20),
                  child: ExpansionTile(
                      expandedCrossAxisAlignment: CrossAxisAlignment.start,
                      expandedAlignment: Alignment.centerLeft,
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            color: currentCategory.key.color,
                            width: 5,
                            height: 50,
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Text(
                            currentCategory.key.title,
                            style: TextStyle(
                                color: Color(0xFF252733),
                                fontSize: 15,
                                fontWeight: FontWeight.w500),
                          ),
                          Spacer(),
                          Text(totalCategoryQuantity.toString(),
                              style: TextStyle(
                                  color: Color(0xFF252733),
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500)),
                        ],
                      ),
                      children: currentCategory.value
                          .map((customService) => Container(
                                padding: EdgeInsets.only(
                                    top: 12, bottom: 12, left: 30, right: 30),
                                decoration: BoxDecoration(
                                  color: Color(0xFFF7F8FC),
                                  border: Border(
                                      bottom:
                                          BorderSide(color: Color(0xFFE3E7F6))),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Flexible(
                                        child: AutoSizeText(
                                            customService.service.name)),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        InkWell(
                                            onTap: () {
                                              cubit.removeQuantity(
                                                  customService,
                                                  currentCategory.key);
                                            },
                                            child: Icon(
                                              Icons.remove_circle_outline,
                                              color: Colors.grey,
                                            )),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                            //Выравнивание, если по шиирине если количество меньше 2х символов.
                                            customService.quantity < 10
                                                ? ' ${customService.quantity} '
                                                : customService.quantity
                                                    .toString(),
                                            style: TextStyle(
                                                color: Color(0xFF60626E),
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500)),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        InkWell(
                                            onTap: () {
                                              cubit.addQuantity(customService,
                                                  currentCategory.key);
                                            },
                                            child: Icon(
                                                Icons.add_circle_outline,
                                                color: Colors.grey))
                                      ],
                                    )
                                  ],
                                ),
                              ))
                          .toList()),
                ),
              ),
            );
          },
        ));
  }

  Widget makeActivePersonOrServiceWidget(
      String currentPerson, String changeButtonText, Function() onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Padding(
            padding: const EdgeInsets.only(left: 16, top: 16, bottom: 16),
            child: Text(
              currentPerson,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
          ),
        ),
        Flexible(
          child: Padding(
            padding: const EdgeInsets.only(right: 12),
            child: InkWell(
              onTap: () {
                onTap();
              },
              child: Text(
                changeButtonText,
                style: TextStyle(color: Color(0xFF1890FF), fontSize: 14),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget makeAddPersonOrServiceWidget(String placeHolderText) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(10),
          child: Container(
            decoration: BoxDecoration(
                color: Color(0xFF1890FF),
                borderRadius: BorderRadius.circular(4)),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Icon(Icons.add, color: Colors.white),
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 16, top: 16, bottom: 16),
          child: Text(
            placeHolderText,
            style: TextStyle(
                color: Colors.black, fontSize: 16, fontWeight: FontWeight.w600),
          ),
        ),
      ],
    );
  }
}
