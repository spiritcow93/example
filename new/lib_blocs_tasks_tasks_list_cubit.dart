import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_atoms/flutter_atoms.dart';
import 'package:helpcare_admin_portal/blocs/tasks/tasks_list_item_cubit.dart';
import 'package:helpcare_admin_portal/models/api.dart';
import 'package:helpcare_admin_portal/models/tasks/task.dart';
import 'package:helpcare_admin_portal/providers/cached_addresses_provider.dart';
import 'package:helpcare_admin_portal/providers/cached_category_provider.dart';
import 'package:helpcare_admin_portal/providers/cached_persons_provider.dart';
import 'package:helpcare_admin_portal/providers/cached_services_provider.dart';
import 'package:injectable/injectable.dart';

part 'tasks_list_state.dart';

@singleton
class TasksListCubit extends Cubit<TasksListState> {
  final API api;
  final CachedPersonsProvider personsProvider;
  final CachedServicesProvider serviceProvider;
  final CachedCategoryProvider categoryProvider;
  final CachedAddressesProvider addressesProvider;

  static final _logger = Logger("TasksListCubit");

  TasksListItemCubit getTaskListItemCubit(String personId) => state.data.taskListItemCubits.putIfAbsent(personId, () => TasksListItemCubit(api));

  TasksListCubit(this.api, this.personsProvider, this.serviceProvider,
      this.categoryProvider, this.addressesProvider)
      : super(TasksListState(TasksListData(), TasksListStateType.INIT));

  Future<void> changeFilterType(TaskListFilterType filterType) async {
    emit(TasksListState(state.data, TasksListStateType.LOADING));
    state.data.filterType = filterType;
    state.data.isLastPage = false;
    state.data.personasTitlesList.clear();
    emit(TasksListState(state.data, TasksListStateType.LOADED));
  }

  Future<void> refresh() async {
    emit(TasksListState(state.data, TasksListStateType.LOADING));
    state.data.isLastPage = false;
    state.data.personasTitlesList.clear();
    emit(TasksListState(state.data, TasksListStateType.LOADED));
  }

  Future<void> changeSorting() async {
    emit(TasksListState(state.data, TasksListStateType.LOADING));
    state.data.ordering = state.data.ordering == TaskListOrderType.START_DATE
        ? TaskListOrderType.MINUS_START_DATE
        : TaskListOrderType.START_DATE;
    state.data.openedTaskListItemCubits.forEach((key, value) {
      if (value.state.data.personTasks.isNotEmpty)
        value.state.data.personTasks.clear();
      value.getTasks(id: key);
    });

    emit(TasksListState(state.data, TasksListStateType.LOADED));
  }

  Future<void> deleteTask(TaskModel task) async {
    emit(TasksListState(state.data, TasksListStateType.CARDS_LOADING));

    final String personaId =
        state.data.filterType == TaskListFilterType.BY_CLIENT_NAME
            ? task.client!.id
            : task.employee!.id;

    try {
      var _api = api.mainApi;
      await _api.mainRestV1TasksDestroy(task.id);

      TasksListItemCubit? taskListItemCubit =
          state.data.taskListItemCubits[personaId];
      taskListItemCubit!.getTasks(id: personaId);

      emit(TasksListState(state.data, TasksListStateType.CARDS_LOADED));
    } catch (e) {
      emit(TasksListState(state.data, TasksListStateType.ERROR, error: e));
    }
  }

  Future<void> getPersonaTitlesList(
      {int pageNumber = 1, int pageSize = 10}) async {
    emit(TasksListState(state.data, TasksListStateType.TITLES_LOADING));
    _logger.info("getPersonaTitlesList");
    try {
      var _api = api.mainApi;
      var response;
      if (state.data.filterType == TaskListFilterType.BY_CLIENT_NAME) {
        response = await _api.mainRestV1ClientsTitlesList(
            ordering: 'name',
            pageLeftSquareBracketNumberRightSquareBracket: pageNumber,
            pageLeftSquareBracketSizeRightSquareBracket: pageSize);
      } else {
        response = await _api.mainRestV1EmployeesTitlesList(
            ordering: 'name',
            pageLeftSquareBracketNumberRightSquareBracket: pageNumber,
            pageLeftSquareBracketSizeRightSquareBracket: pageSize);
      }
      state.data.personasTitlesList = response.data!.results.toList();
      state.data.isLastPage = state.data.personasTitlesList.length < pageSize;
    } catch (e, stacktrace) {
      _logger.severe("error during getPersonaTitlesList", e, stacktrace);
      state.data.isLastPage = true;
      emit(TasksListState(state.data, TasksListStateType.ERROR, error: e));
    }
    emit(TasksListState(state.data, TasksListStateType.TITLES_LOADED));
  }
}
