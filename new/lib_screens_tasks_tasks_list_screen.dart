import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_atoms/flutter_atoms.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:helpcare_admin_portal/blocs/tasks/tasks_list_cubit.dart';
import 'package:helpcare_admin_portal/blocs/tasks/tasks_list_item_cubit.dart';
import 'package:helpcare_admin_portal/constants/constants.dart';
import 'package:helpcare_admin_portal/delegates/help_care_search_delegate.dart';
import 'package:helpcare_admin_portal/generated/l10n.dart';
import 'package:helpcare_admin_portal/ui/buttons.dart';
import 'package:helpcare_admin_portal/ui/pop_up_avatar.dart';
import 'package:helpcare_admin_portal/ui/task_card.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../setup.dart';

//todo: Верстка
class TasksListScreen extends StatefulWidget {
  final TasksListCubit tasksListScreenCubit;

  @override
  _TasksListScreenState createState() =>
      _TasksListScreenState(tasksListScreenCubit);

  TasksListScreen(this.tasksListScreenCubit);
}

class _TasksListScreenState extends State<TasksListScreen> {
  final TasksListCubit tasksListCubit;

  _TasksListScreenState(this.tasksListCubit);


  static const _pageSize = 10;

  ScrollController? _personaListScrollController;

  final PagingController<int, dynamic> _pagingController =
      PagingController(firstPageKey: 1, invisibleItemsThreshold: 1);

  _fetchPage(int pageKey) async {
    try {
      await tasksListCubit.getPersonaTitlesList(
          pageSize: _pageSize, pageNumber: pageKey);
      if (tasksListCubit.state.data.isLastPage) {
        _pagingController
            .appendLastPage(tasksListCubit.state.data.personasTitlesList);
      } else {
        final nextPageKey = pageKey + 1;
        _pagingController.appendPage(
            tasksListCubit.state.data.personasTitlesList, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  void initState() {
    _personaListScrollController = ScrollController();
    tasksListCubit.state.data.isLastPage = false;
    _pagingController.refresh();
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  TextStyle blackText = TextStyle(
      color: Color(0xFF252733), fontSize: 14, fontWeight: FontWeight.w700);


  @override
  Widget build(BuildContext context) {
    if(_personaListScrollController!.hasClients) {
      _personaListScrollController?.jumpTo(_personaListScrollController!.position.minScrollExtent);
    }

    return Title(
      title: S.of(context).tasksTitle,
      color: Colors.green,
      child: Scaffold(
        appBar: AppBar(
          title: SearchBar(HelpCareSearchDelegate(hostScreenTitle: "Пользователи", taskSearchBloc: sl()), "Поиск"),
          actions: [PopUpAvatar()]),
        body: BlocConsumer<TasksListCubit, TasksListState>(
          bloc: tasksListCubit,
          listener: (context, state) {
            if (state.type == TasksListStateType.LOADED) {
              _personaListScrollController!.jumpTo(
                  _personaListScrollController!
                      .position.minScrollExtent);
              _pagingController.refresh();
            }
          },
          builder: (context, state) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                makeHeaderBlock(state),
                makeSortWidget(),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: personasListWidget(state.data.personasTitlesList,
                        context, state.data.filterType),
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }

  Widget makeHeaderBlock(TasksListState state) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Задания',
            style: TextStyle(
                color: Color(0xFF252733),
                fontSize: 22,
                fontWeight: FontWeight.w500),
          ),
          filterButtonsWidget(state.data.filterType, context),
          makeAddTaskWidget(context),
        ],
      ),
    );
  }

  Widget makeRowWithEmployeeAndDepartmentForCard(String icon, String text) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, bottom: 10),
      child: Row(
        children: [
          SvgPicture.asset(icon),
          SizedBox(
            width: 10,
          ),
          AutoSizeText(
            text,
            style: TextStyle(color: Color(0xFF898CA1), fontSize: 14),
          )
        ],
      ),
    );
  }

  Widget makeQuantityChipWidget(
      {required String label,
      required Color bgColor,
      required String avatarUrl,
      required BuildContext context}) {
    return Padding(
      padding: const EdgeInsets.only(top: 12),
      child: Chip(
        backgroundColor: bgColor,
        labelPadding: EdgeInsets.only(
            right: MediaQuery.of(context).size.width * 0.015,
            left: MediaQuery.of(context).size.width * 0.015),
        padding: EdgeInsets.only(top: 5, bottom: 5, right: 10, left: 5),
        avatar: SvgPicture.asset(avatarUrl),
        label: Text(
          label,
          style: TextStyle(
              color: Colors.white, fontSize: 14, fontWeight: FontWeight.w700),
        ),
      ),
    );
  }

  Widget makeSortWidget() {
    return InkWell(
      onTap: () => tasksListCubit.changeSorting(),
      child: Row(
        children: [
          Icon(
            tasksListCubit.state.data.ordering ==
                    TaskListOrderType.START_DATE
                ? Icons.arrow_drop_up
                : Icons.arrow_drop_down,
            color: Color(0xFF9FA2B4),
          ),
          Text(
            'По дате начала',
            style: TextStyle(color: Color(0xFF9FA2B4), fontSize: 14),
          )
        ],
      ),
    );
  }

  Widget makeAddTaskWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 25),
      child: InkWell(
        onTap: () {
          Routes.tasksCreate.go();
        },
        child: Row(
          children: [
            Container(
              decoration: BoxDecoration(
                  color: Color(0xFF1890FF),
                  borderRadius: BorderRadius.circular(4)),
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(6),
                  child: Icon(Icons.add, color: Colors.white),
                ),
              ),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(4),
                  border: Border.all(color: Color(0xFFE3E7F6)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 16, top: 10, bottom: 10),
                  child: Text(
                    'Добавить задание',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.w500),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget personasListWidget(
      List<dynamic> personasList,
      BuildContext context,
      TaskListFilterType filterType) {
    return PagedListView<int, dynamic>(
      scrollController: _personaListScrollController,
      pagingController: _pagingController,
      builderDelegate: PagedChildBuilderDelegate<dynamic>(
          noItemsFoundIndicatorBuilder: (context) => Padding(
                padding: const EdgeInsets.all(12),
                child: Align(
                    alignment: Alignment.topCenter,
                    child: filterType == TaskListFilterType.BY_CLIENT_NAME
                        ? Text('Список клиентов пуст')
                        : Text('Список сотрудников пуст')),
              ),
          noMoreItemsIndicatorBuilder: (context) => personasList.isNotEmpty
              ? Align(
                  alignment: Alignment.topCenter,
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text('Вы загрузили весь список'),
                  ))
              : Container(),
          itemBuilder: (context, persona, index) {
            var persistenceCubit = tasksListCubit.getTaskListItemCubit(persona.id);
            return BlocBuilder<TasksListItemCubit, TasksListItemState>(
              bloc: persistenceCubit,
              builder: (context, state) {
                if(state.type == TasksListItemStateType.NEED_UPDATE) {
                  persistenceCubit.getTasks(id: persona.id);
                }
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: Theme(
                    data: ThemeData().copyWith(
                      dividerColor: Colors.white,
                      hoverColor: Colors.white,
                      highlightColor: Colors.white,
                      splashColor: Colors.white,
                    ),
                    child: Material(
                      elevation: 5,
                      child: ExpansionTile(
                          maintainState: false,
                          collapsedBackgroundColor: Colors.white,
                          initiallyExpanded: false,
                          backgroundColor: Colors.white,
                          expandedCrossAxisAlignment: CrossAxisAlignment.start,
                          expandedAlignment: Alignment.centerLeft,
                          onExpansionChanged: (opened) {
                            if (opened) {
                              tasksListCubit.state.data.openedTaskListItemCubits.putIfAbsent(persona.id, () => persistenceCubit);
                              persistenceCubit.getTasks(id: persona.id);

                            } else {
                              tasksListCubit.state.data.openedTaskListItemCubits.remove(persona.id);
                              state.data.personTasks.remove(persona.id);
                            }
                          },
                          leading: SvgPicture.asset(
                            'assets/tasks/task_ppl.svg',
                            width: 14,
                            height: 14,
                          ),
                          title: Text(
                            persona.name,
                            style: TextStyle(
                                fontSize: 15,
                                color: Color(0xFF252733),
                                fontWeight: FontWeight.w500),
                          ),
                          children: [
                            state.data.personTasks[persona!.id] != null &&  state.data.personTasks[persona!.id]!.isNotEmpty && persistenceCubit.state.type ==
                                    TasksListItemStateType.CARDS_LOADED
                                ? Padding(
                                    padding: const EdgeInsets.only(left: 20),
                                    child: GridView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        gridDelegate:
                                            SliverGridDelegateWithMaxCrossAxisExtent(
                                              maxCrossAxisExtent: 390,
                                              crossAxisSpacing: 15,
                                              mainAxisSpacing: 15,
                                              childAspectRatio: 1,
                                        ),
                                        itemCount:
                                        state.data.personTasks[persona!.id]!.length,
                                        itemBuilder: (BuildContext ctx, index) {
                                          var task = state.data.personTasks[
                                              persona!.id]![index];
                                          return TaskCard(task);
                                        }),
                                  )
                                : persistenceCubit.state.type ==
                                TasksListItemStateType.CARDS_LOADED
                                    ? Padding(
                                        padding: const EdgeInsets.all(20),
                                        child: Text(
                                          'Нет заданий',
                                          style: TextStyle(
                                              color: Color(0xFF252733),
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      )
                                    : persistenceCubit.state.type ==
                                TasksListItemStateType.CARDS_LOADING
                                        ? Padding(
                                            padding: const EdgeInsets.all(20),
                                            child: Center(
                                                child:
                                                    CircularProgressIndicator()),
                                          )
                                        : Container(),
                          ]),
                    ),
                  ),
                );
              },
            );
          }),
    );
  }

  Widget filterButtonsWidget(
      TaskListFilterType selectedFilterType, BuildContext context) {
    return Wrap(
        children: TaskListFilterType.values
            .map((filterType) => Padding(
                  padding: const EdgeInsets.only(right: 10.0, top: 30),
                  child: ChoiceChip(
                    shape: RoundedRectangleBorder(
                      side: BorderSide(color: Color(0xFF1890FF)),
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    backgroundColor: Colors.white,
                    labelPadding: EdgeInsets.symmetric(horizontal: 20.0),
                    selectedColor: Color(0xFF1890FF),
                    label: Text(
                      filterType.toLangString(),
                      style: TextStyle(
                          color: selectedFilterType == filterType
                              ? Colors.white
                              : Color(0xFF1890FF),
                          fontSize: 12),
                    ),
                    selected: selectedFilterType == filterType,
                    onSelected: (bool selected) {
                      tasksListCubit.changeFilterType(filterType);
                    },
                  ),
                ))
            .toList());
  }
}
