part of 'detailed_client_service_cubit.dart';

enum DetailedClientServiceStateType { INIT, LOADING, LOADED, ERROR }

class DetailedClientServiceData {
  List<ClientNamedService> clientServices = [];
  DateTime startDate;
  DateTime endDate;
  int currentIndex;
}

@immutable
class DetailedClientServiceState {
  final DetailedClientServiceData data;
  final DetailedClientServiceStateType type;
  DetailedClientServiceState(this.data, this.type);
}
